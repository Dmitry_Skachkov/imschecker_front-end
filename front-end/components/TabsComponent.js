import React from "react";
import ReactDOM from "react-dom";
import {Col, Tab, Tabs} from "react-materialize";

export default class TabsComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFixed: false,
        };

        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
        this.props.setTabsOffset(this.offset);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        let scrollTop = document.documentElement.scrollTop;

        this.setState({isFixed: scrollTop > this.offset.top});

    }

    render() {
        return (
            <Col s={12} className={this.state.isFixed ? 'tab-template-fixed' : 'tab-template'}>
                <Col s={10} offset='s1'>

                    <Tabs className='tabs tabs-fixed-width'>
                        <Tab title="PCFA"></Tab>
                        <Tab title="PCOA" active></Tab>
                        <Tab title="SOF"></Tab>
                        <Tab title="CFun"></Tab>
                        <Tab title="FTCM"></Tab>
                        <Tab title="PAFE"></Tab>
                        <Tab title="All"></Tab>
                    </Tabs>
                </Col>
            </Col>
        );
    }
}