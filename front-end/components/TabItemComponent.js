import React from "react";
import ReactDOM from "react-dom";

export default class TabItemComponent extends React.Component {

    componentDidUpdate() {
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
        this.props.onShowContent(this.offset);
    }

    render() {
        return (
          <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies vel velit eget bibendum. Nulla
              porttitor nunc vitae neque tincidunt eleifend. Aenean scelerisque iaculis ante porttitor mollis. Nunc
              quam lectus, iaculis et eros eu, finibus accumsan lacus. Proin a orci sagittis, porta enim at, feugiat
              ante. Nam auctor felis sit amet tincidunt pharetra. Morbi aliquam, ante ac iaculis dignissim, arcu odio
              ullamcorper turpis, lobortis bibendum mi nisl quis purus. Donec vel nulla erat.

              {/*Integer tempor facilisis lacus sed fringilla. Curabitur congue gravida sem id eleifend. Mauris et*/}
              {/*aliquet nisl. Pellentesque tristique lacus sit amet enim volutpat, eu tincidunt mauris malesuada. Etiam*/}
              {/*tincidunt ante nec libero tempus sagittis. Aliquam eu tristique diam. Nullam sodales libero nec leo*/}
              {/*porttitor, a scelerisque magna feugiat. Ut laoreet elementum risus, ac pretium diam luctus eget. Integer*/}
              {/*sed purus sagittis, egestas sem ut, gravida massa. Praesent vel laoreet arcu, vel tempus turpis.*/}
              {/*Praesent ut ligula gravida, mollis nunc vel, rutrum ante. Morbi malesuada, sem sit amet semper molestie,*/}
              {/*urna odio maximus est, eu hendrerit massa lacus non ligula. Donec laoreet purus sit amet justo*/}
              {/*scelerisque fringilla. Etiam convallis pharetra metus non blandit.*/}

              {/*Praesent euismod felis diam, mollis tincidunt lectus imperdiet quis. Nam tincidunt velit at lacus*/}
              {/*euismod, non iaculis quam placerat. Donec eu hendrerit nisi. Proin dapibus tellus et cursus*/}
              {/*pellentesque. Donec efficitur neque vestibulum nisl pharetra tincidunt. Cras sed dignissim arcu, ut*/}
              {/*pharetra magna. In ullamcorper diam accumsan nisl porta faucibus.*/}

              {/*Nam tempus neque eu tempus maximus. Curabitur eget convallis lacus. In sit amet felis ac dui euismod*/}
              {/*tincidunt ut a augue. Duis vestibulum maximus enim sed consectetur. Nullam porttitor eu nibh quis*/}
              {/*mattis. Nulla vitae condimentum lacus, vel ultrices arcu. Praesent ornare rhoncus orci et vestibulum.*/}
              {/*Nulla facilisi. Maecenas pellentesque gravida rutrum. Pellentesque pretium tortor sed posuere tristique.*/}
              {/*Aliquam tincidunt lorem faucibus enim dictum, ut hendrerit ante lacinia. Phasellus scelerisque urna eget*/}
              {/*ligula scelerisque dapibus. Sed a lorem eget sem cursus volutpat quis eu lacus. Nunc at ipsum fringilla,*/}
              {/*varius sapien eu, varius mauris. Sed placerat ultricies neque, a lobortis nisi pretium a. Cras sed*/}
              {/*lobortis velit.*/}

              {/*Phasellus malesuada auctor metus nec vulputate. Sed aliquet elementum molestie. Aliquam pretium urna*/}
              {/*lacus, in vestibulum enim dictum vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam*/}
              {/*in risus bibendum, scelerisque diam nec, commodo mi. Fusce a sapien facilisis, laoreet ligula non,*/}
              {/*mollis justo. Sed eu semper arcu. Aenean nec euismod nunc.*/}

              {/*Mauris sit amet odio pellentesque, pharetra lacus ut, blandit sem. Fusce lacinia ultricies tincidunt.*/}
              {/*Duis at ullamcorper ante. Duis nec nisl ac neque porta facilisis et quis urna. Pellentesque quis*/}
              {/*convallis mauris, et eleifend mi. Phasellus nec erat egestas, auctor est in, mollis nulla. Morbi sapien*/}
              {/*eros, imperdiet nec suscipit non, efficitur ut sem. Nunc vitae massa condimentum, pulvinar magna id,*/}
              {/*vehicula nulla. Nunc eu dui lorem. In vulputate mi purus, consectetur fermentum tortor ultrices ut.*/}
              {/*Etiam non pulvinar nisl. Vestibulum nulla odio, ullamcorper vitae nunc sed, pellentesque faucibus nunc.*/}
              {/*Cras sollicitudin consequat elit. Etiam neque diam, fermentum quis semper nec, aliquam nec urna.*/}

              {/*Aenean dolor nibh, venenatis eget rhoncus eget, pharetra et tortor. Pellentesque eu tempus sem. Maecenas*/}
              {/*non pretium ligula. Donec in turpis eget ipsum molestie efficitur. Cras vel maximus arcu. Ut congue*/}
              {/*mauris dolor, in placerat arcu dignissim et. Cras ultricies arcu quis nibh pulvinar, a cursus odio*/}
              {/*aliquam. Integer vehicula, eros sed pharetra aliquet, lacus elit venenatis neque, sit amet fermentum*/}
              {/*odio erat eget arcu. Phasellus at lorem pharetra augue laoreet luctus. Praesent semper eu est venenatis*/}
              {/*efficitur. Ut accumsan luctus libero, laoreet consequat sem faucibus eu. Sed dictum velit vitae quam*/}
              {/*dapibus, eget cursus mi finibus. Mauris vestibulum pulvinar metus, et auctor metus vehicula quis.*/}

              {/*Nam tortor est, malesuada a tellus ut, gravida porttitor turpis. Vivamus massa diam, varius sit amet*/}
              {/*ipsum eget, euismod posuere augue. In pretium, est quis rhoncus finibus, nisl ex rhoncus erat, viverra*/}
              {/*interdum sem elit nec eros. Proin a augue ex. Sed volutpat consequat metus quis vestibulum. Sed quam*/}
              {/*nisl, porta eu laoreet at, cursus et nulla. Proin lacinia nisl et ligula pellentesque faucibus.*/}
              {/*Curabitur a tristique dolor. Donec semper sagittis tellus, vel imperdiet ligula placerat at. Proin*/}
              {/*egestas magna a justo pulvinar, at tempus diam porttitor. Nullam pulvinar mi et nibh mattis ornare.*/}
              {/*Phasellus condimentum urna sit amet diam luctus, sed tristique enim tincidunt. Ut ac risus rutrum,*/}
              {/*ornare erat ac, imperdiet nulla.*/}

              {/*Nullam quis vestibulum ligula, sed blandit mi. Aenean magna est, convallis vitae libero id, condimentum*/}
              {/*iaculis nulla. Nulla justo diam, rutrum ac orci vel, pretium sollicitudin nisi. Nulla vestibulum, felis*/}
              {/*non finibus convallis, leo nulla malesuada quam, dapibus consectetur risus erat sed quam. Pellentesque*/}
              {/*habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras mollis consequat*/}
              {/*lacus, sodales dapibus nulla elementum a. Proin vehicula urna vitae lectus sodales semper. Suspendisse*/}
              {/*quis urna vestibulum, ornare purus eu, varius lorem. Integer efficitur porta aliquet.*/}

              {/*Donec tincidunt, nisi eu ultrices aliquam, tortor lorem imperdiet arcu, id congue quam libero vitae*/}
              {/*eros. Suspendisse in elit at mauris maximus blandit vitae imperdiet arcu. Maecenas placerat luctus*/}
              {/*augue, sit amet vehicula orci luctus tincidunt. Mauris auctor arcu eget magna dictum, sit amet consequat*/}
              {/*nulla posuere. Phasellus luctus dapibus ipsum, in tincidunt tellus porta a. Curabitur fermentum*/}
              {/*condimentum lectus eu tempor. Vestibulum id odio pretium, consectetur eros vitae, accumsan nisl. Integer*/}
              {/*nunc ipsum, malesuada eget ultrices vitae, convallis vel neque. Pellentesque et vehicula ipsum, in*/}
              {/*mattis nisi. Curabitur ac sollicitudin velit, vitae dictum ligula. Duis laoreet eros vestibulum*/}
              {/*facilisis egestas.*/}

              {/*Nunc lobortis rhoncus diam, quis condimentum leo rutrum et. Pellentesque laoreet ultrices orci, vel*/}
              {/*interdum odio congue eget. Maecenas dignissim blandit tortor eget tempus. Donec in vehicula nisi, quis*/}
              {/*condimentum velit. Vestibulum laoreet nulla vel leo aliquam, nec sodales velit bibendum. Morbi mi orci,*/}
              {/*elementum non maximus nec, congue ac turpis. Morbi ullamcorper diam ac iaculis accumsan. Vivamus mollis*/}
              {/*consequat mauris ac semper. Vivamus libero diam, rhoncus ac tortor quis, eleifend fringilla nisi. Etiam*/}
              {/*sagittis rhoncus quam a maximus. Duis vitae aliquam turpis.*/}

              {/*Pellentesque porta bibendum risus non posuere. Maecenas non tellus vel libero varius convallis. Proin a*/}
              {/*condimentum odio. Nulla sed neque odio. Fusce vitae est a nibh tristique vehicula eget sed massa. Donec*/}
              {/*sollicitudin nibh id libero condimentum, feugiat laoreet nibh scelerisque. Duis facilisis hendrerit*/}
              {/*sapien.*/}

              {/*Vivamus ut metus nec orci iaculis vestibulum at eget arcu. Pellentesque lacinia justo id orci porttitor*/}
              {/*commodo. Aenean id posuere arcu. Duis ultricies dolor metus, quis suscipit nisi vestibulum eget. Cras*/}
              {/*dictum suscipit leo, in efficitur ante mollis vitae. Nulla lacinia varius nibh sed facilisis. Etiam*/}
              {/*hendrerit a ligula eget posuere. Duis id varius velit.*/}

              {/*Proin accumsan ligula nec nibh posuere, a luctus ante vulputate. Pellentesque habitant morbi tristique*/}
              {/*senectus et netus et malesuada fames ac turpis egestas. Morbi non felis nec massa tempus sagittis. Nulla*/}
              {/*blandit aliquet pharetra. Fusce et lobortis elit. Curabitur tempus, lorem at condimentum finibus, est*/}
              {/*lacus pharetra massa, at congue arcu urna id purus. Fusce vitae risus eros.*/}

              {/*Morbi ultricies posuere maximus. Aenean bibendum sem vitae sagittis pharetra. Vestibulum ultricies ante*/}
              {/*ac sapien malesuada, dictum interdum nisl tempus. Phasellus cursus non neque ut lacinia. Sed condimentum*/}
              {/*nunc velit, at convallis turpis pellentesque et. Nam porttitor felis eu leo dignissim, eget hendrerit*/}
              {/*nisl sagittis. Cras luctus augue accumsan elementum euismod. Vivamus maximus, eros id venenatis*/}
              {/*fringilla, sem tortor lacinia eros, in rutrum dolor nisi quis nulla. Mauris enim dolor, cursus at*/}
              {/*interdum ac, cursus id lacus. Etiam finibus felis fringilla accumsan euismod.*/}

              {/*In fermentum, erat feugiat luctus ullamcorper, nibh orci dapibus sem, et rutrum lacus diam nec tortor.*/}
              {/*Aenean suscipit dolor ut purus vehicula, non dapibus lorem malesuada. Sed luctus aliquam vulputate.*/}
              {/*Nulla facilisi. Pellentesque sed sapien ut odio dictum bibendum. Fusce viverra, elit et ullamcorper*/}
              {/*iaculis, nunc risus volutpat felis, in hendrerit diam erat eu augue. Nulla viverra orci vel venenatis*/}
              {/*rutrum.*/}

              {/*Vivamus rhoncus vulputate nulla at dignissim. Duis malesuada pulvinar justo, at volutpat ligula gravida*/}
              {/*a. Praesent vulputate facilisis mi vel fermentum. Donec mi lorem, faucibus quis dictum pellentesque,*/}
              {/*dapibus sed massa. Etiam at ligula erat. Etiam elementum purus id odio viverra, vel molestie lectus*/}
              {/*fermentum. Pellentesque in libero sit amet nunc scelerisque ornare. Nunc auctor, sapien id scelerisque*/}
              {/*pulvinar, mauris dolor posuere leo, nec faucibus enim elit et justo. Vivamus ac fermentum sapien.*/}
              {/*Phasellus erat massa, ultricies id laoreet ut, vulputate sed massa. Nulla vel ante a lorem aliquet*/}
              {/*feugiat sed sit amet mi.*/}

              {/*Integer eget turpis felis. Suspendisse libero dolor, viverra ut tempus ut, porttitor sit amet felis.*/}
              {/*Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam convallis*/}
              {/*sapien lorem, vel luctus arcu tempus convallis. Nullam non lectus quis quam luctus vulputate quis sit*/}
              {/*amet ipsum. Proin suscipit enim non sem rutrum, ac mollis ipsum vestibulum. Quisque aliquet nisl lorem,*/}
              {/*eu congue neque efficitur sed. Proin aliquet ipsum iaculis odio pellentesque tristique. Aliquam non*/}
              {/*ornare turpis, et aliquet tellus. Pellentesque sed sodales lectus, quis ultricies eros.*/}

              {/*Quisque dictum tincidunt dui sit amet lobortis. Quisque auctor fringilla lacus at lacinia. Fusce*/}
              {/*finibus, libero id rutrum congue, turpis nibh egestas augue, id fringilla enim leo in augue. Praesent*/}
              {/*sagittis semper ante vel interdum. Fusce lobortis dictum orci sed pharetra. Aliquam erat volutpat. Ut*/}
              {/*feugiat ipsum in velit placerat, non euismod velit vulputate. Aliquam sem tellus, tristique et eleifend*/}
              {/*sed, feugiat quis tellus. Phasellus posuere vestibulum quam facilisis eleifend. Quisque ac semper felis.*/}
              {/*Integer dictum, magna sit amet convallis pellentesque, erat mauris cursus magna, sit amet rutrum quam*/}
              {/*diam eget justo. Mauris ut pretium sem, nec elementum diam. Morbi pulvinar, leo sit amet porttitor*/}
              {/*lacinia, eros augue fermentum ipsum, a pharetra massa dui consequat sapien. Duis erat orci, finibus a*/}
              {/*condimentum nec, euismod in leo.*/}

              {/*Praesent imperdiet sodales neque, et mattis nulla faucibus id. Integer non nunc id quam porttitor*/}
              {/*consectetur. Sed nibh risus, placerat in nibh quis, convallis commodo urna. Proin varius quis sem non*/}
              {/*facilisis. Nunc ultrices eros lorem, ut rutrum nunc pellentesque maximus. Morbi sed sem in lectus dictum*/}
              {/*suscipit eu quis arcu. Cras vitae enim eget velit sollicitudin accumsan a a felis. Quisque congue augue*/}
              {/*nunc, quis imperdiet velit rhoncus non.*/}
          </div>
        );
    }
}