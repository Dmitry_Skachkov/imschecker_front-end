import React from "react";
import ReactDOM from "react-dom";
import {CollapsibleItem} from "react-materialize";

import TabItemComponent from "./TabItemComponent";

export default class CollapsibleItemComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFixed: false,
        };

        this.handleScroll = this.handleScroll.bind(this);
        this.onShowContent = this.onShowContent.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
        this.tabsOffset = this.props.getTabsOffset();
        this.fixedTopOffset = this.offset.top - this.tabsOffset.top - 1;

    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        let scrollTop = document.documentElement.scrollTop;

        if(scrollTop > this.tabsOffset.top) {
            ReactDOM.findDOMNode(this).firstChild.className = 'collapsible-header collapsible-header-fixed';
            if(this.props.addBorder) {
                ReactDOM.findDOMNode(this).firstChild.style.borderTop = '2px solid #ddd';
            }
            ReactDOM.findDOMNode(this).firstChild.style.width = this.offset.width + 'px';
            ReactDOM.findDOMNode(this).firstChild.style.top = this.fixedTopOffset + 'px';
            ReactDOM.findDOMNode(this).firstChild.style.marginTop = '-0.5rem';
        } else {
            ReactDOM.findDOMNode(this).firstChild.className = 'collapsible-header';
            ReactDOM.findDOMNode(this).firstChild.style.marginTop = '0rem';
        }

        this.setState({isFixed: scrollTop > this.tabsOffset.top});
    }

    componentDidUpdate() {
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
        console.log('collapse did update', this.offset);
        console.log('collapse did update', ReactDOM.findDOMNode(this).firstChild.offsetTop);

    }

    onShowContent(itemOffset) {
        //console.log(window.innerHeight)

        console.log('onShowContent');
        // console.log(ReactDOM.findDOMNode(this).firstChild.offsetTop);
        // console.log(ReactDOM.findDOMNode(this).firstChild);

        // console.log('onShowContent', itemOffset);
        // console.log('onShowContent', this.offset);
    }

    render() {
        return (
            <CollapsibleItem
              header={this.props.header}
              // className={this.state.isFixed ? 'collapsible-header-fixed' : ''}
              // style={this.state.isFixed ? {width: this.offset.width, top: this.fixedTopOffset, marginTop: '-0.5rem'} : {}}
              icon='filter_drama'
              onSelect={this.props.onSelect}
            >
                <TabItemComponent onShowContent={this.onShowContent}/>
            </CollapsibleItem>
        );
    }
}