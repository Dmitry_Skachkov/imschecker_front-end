import React from "react";
import ReactDOM from "react-dom";
import {Collapsible, CollapsibleItem} from "react-materialize";

import TabItemComponent from "./TabItemComponent";
import CollapsibleItemComponent from "./CollapsibleItemComponent";

export default class TabComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFixed: false,
        };
    }

    componentDidMount() {
        this.tabsOffset = this.props.getTabsOffset();
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
    }

    componentWillUpdate() {
        this.offset = ReactDOM.findDOMNode(this).getBoundingClientRect();
        console.log('will update ', this.offset);
    }

    render() {
        return (
            <Collapsible accordion>
                <CollapsibleItemComponent header="Transactions" getTabsOffset={this.props.getTabsOffset} addBorder={true}/>
                <CollapsibleItemComponent header="BMP" getTabsOffset={this.props.getTabsOffset}/>

                <CollapsibleItem header='PSB' icon='whatshot'>
                    Lorem ipsum dolor sit amet.
                </CollapsibleItem>
                <CollapsibleItem header='PSB' icon='whatshot'>
                    {/*<TabItemComponent/>*/}
                </CollapsibleItem>
            </Collapsible>
        );
    }
}