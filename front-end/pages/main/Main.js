import React from "react";
import {Col, Row} from "react-materialize";

import "./main.css";
import TabsComponent from "../../components/TabsComponent";
import TabComponent from "../../components/TabComponent";

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isTabFixed: false,
        };

        this.setTabsOffset = this.setTabsOffset.bind(this);
        this.getTabsOffset = this.getTabsOffset.bind(this);
    }

    setTabsOffset(offset) {
        this.tabsOffset = offset;
    }

    getTabsOffset() {
        return this.tabsOffset;
    }

    render() {
        return (
            <Row>
                <Col s={12} style={{textAlign: 'center'}}>
                    <div style={{display: 'inline-block'}}>
                        <div className="major-header">IMS online transactions checker</div>
                        <div className="minor-header">For country 866/754 GB & Ireland</div>
                    </div>
                </Col>
                <TabsComponent setTabsOffset={this.setTabsOffset}/>
                <Col s={10} offset='s1'>
                    <TabComponent getTabsOffset={this.getTabsOffset}/>
                </Col>
            </Row>
        );
    }
}