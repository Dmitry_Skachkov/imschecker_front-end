import React from 'react';
import {Route, Redirect} from 'react-router-dom';

import Main from './main/Main';

export default class App extends React.Component {
    render() {
       return (
           <Route exact path="/" component={Main}/>
        );
    }
}