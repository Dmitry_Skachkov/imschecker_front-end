let webpack = require('webpack');
let path = require('path');

let node_dir = __dirname + '/node_modules';

module.exports = {
    entry: './index.js',
    devtool: 'sourcemaps',
    cache: true,
    output: {
        path: __dirname,
        filename: './built/bundle.js'
    },
    plugins: [
        //new webpack.optimize.OccurrenceOrderPlugin(),
        // new webpack.optimize.UglifyJsPlugin({
        //     sourceMap: false,
        //     extractComments: true,
        //     uglifyOptions: {
        //         ie8: false,
        //         ecma: 8,
        //         warnings: false
        //     }
        // }),
    ],
    module: {
        loaders: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react', 'stage-2']
                }
            },
            {test: /\.css$/, loader: "style-loader!css-loader"},
            {
                test: /\.png/,
                loader: 'file-loader',
                options: {
                    name: 'built/[hash].[ext]'
                }
            }
        ],

    }
};